'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefix = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var del = require('del');
var browserSync = require('browser-sync').create();

gulp.task('clean', function() {
  del(['.dist/*', '!dist/.git'], { dot: true });
});

gulp.task('style', function(){
  return gulp.src('src/scss/**/*.scss')
  .pipe(sourcemaps.init())
  .pipe(sass())
  .pipe(autoprefix({
            browsers: ['last 2 versions']

         }))
  .pipe(csso({
            restructure: false,
            sourceMap: true,
            debug: true
       }))
  .pipe(sourcemaps.write())
  .pipe(gulp.dest('.dist'))
});

gulp.task('serve', ['style'], function () {
  browserSync.init({
    server:['.dist', 'src']
  });
  gulp.watch('src/scss/**/*.scss',['style', browserSync.reload]);
  gulp.watch('src/**/*.html',browserSync.reload);
});

gulp.task('default', function(){
  gulp.src([
    'src/**/*'

  ], {
    dot: true,
  })
  .pipe(gulp.dest('.dist'));
});
